var express = require('express');
var bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;

var app = express();


// Importamos el modelo Usuario
var Usuario = require('../models/usuario');

//===============================================
// Obtener Todos los Usuarios
//===============================================

//Rutas
app.get('/',(req, res, next)=>{
  Usuario.find({}, 'nombre email role')
  .exec(
      (err, usuarios)=>{

    if(err){
      return res.status(500).json({
        ok: false,
        mensaje: 'Error cargando usuarios..!',
        errors: err
      });
    }

     res.status(200).json({
      ok: true,
      usuarios: usuarios
    });

  });

});



//===============================================
// Verificar token
//===============================================


app.use('/', (req, res, next)=>{

  //var token = req.query.token;
  var headers = req.headers;
  var token = headers.token;
jwt.verify(token , SEED, (err, decoded) =>{

   if(err){
           return res.status(401).json({
              ok: false,
              mensaje: 'Token no valido',
              errors: err
           });
       }
         next();

   });
});


//===============================================
// Crear un nuevo usuario
//===============================================

app.post('/',(req, res)=>{
  var body = req.body;

  var usuario = new Usuario({
     nombre: body.nombre,
     email: body.email,
     password:  bcrypt.hashSync(body.password, 10),
     img: body.img,
     role: body.role
  });

  usuario.save((err,usuarioGuardado)=>{

         if(err){
               return res.status(400).json({
                 ok: false,
                 mensaje: 'Error al crear usuario.',
                 errors: err
               });
            }

               return res.status(201).json({
               ok: true,
               usuario: usuarioGuardado
            });
     });
});




//===============================================
// Actualizar un Usuario
//===============================================

app.put('/:id', (req, res)=>{

   var id = req.params.id;
   var body = req.body;

   Usuario.findById(id,'nombre email role', (err, usuario)=>{

     if(err){
       return res.status(500).json({
         ok: false,
         mensaje: 'Error actualizando el usuario!',
         errors: err
       });
     }

     if(!usuario){
       return res.status(400).json({
         ok: false,
         mensaje: 'No se encontro el usuario por el id' + id,
         errors: {message: 'No existe un usuario con ese Id'}
       });
     }
           usuario.nombre = body.nombre;
           usuario.email = body.email;
           usuario.rol = body.rol;

          usuario.save((err, usuarioAptualizado)=>{

            if(err){
              return res.status(400).json({
                ok: false,
                mensaje: 'Error al actualizar usuario',
                errors: err
              });
            }

            res.status(200).json({
              ok: true,
              usuario: usuarioAptualizado
            });
        });
     });
 });


 //===============================================
 // Eliminar un Usuario
 //===============================================


app.delete('/borrar/:id', (req, res)=>{

  var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado)=>{

        if (err){
          return res.status(500).json({
           ok: false,
           mensaje: 'Error al borrar al usuario',
           errors: err
         });
        }

        if(!usuarioBorrado){
          return res.status(400).json({
            ok: false,
            mensaje: 'No se encontro el usuario por el id' + id,
            errors: {message: 'No existe un usuario con ese Id'}
          });
        }

         res.status(200).json({
            ok: true,
            usuario: usuarioBorrado
        });

     });
 });


module.exports = app;
