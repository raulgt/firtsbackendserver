var express = require('express');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;

var app = express();

var Usuario = require('../models/usuario');

//===============================================
// Loguear usuario
//===============================================

app.post('/', (req, res)=>{
  var body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB) => {

       if(err){
           return res.status(500).json({
              ok: false,
              mensaje: 'Error al buscar usuarios',
              errors: err
           });
        }

       if(!usuarioDB){
         return res.status(400).json({
           ok: true,
           mensaje: 'Credenciales incorrectas - email',
           errors: err
         });
        }

        if(!bcrypt.compareSync(body.password, usuarioDB.password)){
          return res.status(400).json({
            ok: true,
            mensaje: 'Credenciales incorrectas - password',
            errors: err
          });
        }

        var usuarioRespuesta = {
          nombre: usuarioDB.nombre,
          email: usuarioDB.email,
          rol: usuarioDB.rol,
          img: usuarioDB.img
        }

        //Crear un token
        var token = jwt.sign({usuario: usuarioRespuesta}, SEED,{ expiresIn: '4h' });

        res.status(200).json({
           ok: true,
           usuario: usuarioRespuesta,
           id: usuarioDB._id,
           token: token
        });
      });
   });


module.exports = app;
