var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;


var flowersSchema = new Schema({

  nombre: {type: String, required: [true,'El nombre es necesario..!']},
  email: {type: String, unique: true, require:[true,'El correo es necesario..!']},
  password: {type: String, require:[true,'La contrasena es necesaria..!']},
  img: {type: String, require: false },
  role: {type: String, require: true, default: 'USER_ROLE', enum: rolesValidos}

});



module.exports = mongoose.model('Flowers', flowersSchema);
